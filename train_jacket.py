# Package Includes
from __future__ import division

import os
import socket
import timeit
from datetime import datetime
from tensorboardX import SummaryWriter
import logging

# PyTorch includes
import torch
import torch.optim as optim
from torchvision import transforms
from torch.utils.data import DataLoader

# Custom includes
from dataloaders import davis_2016 as db
from dataloaders import custom_transforms as tr
from util import visualize as viz
import scipy.misc as sm
import networks.vgg_osvos as vo
from layers.osvos_layers import class_balanced_cross_entropy_loss
#from layers.osvos_layers import imbalanced_cross_entropy_loss
from dataloaders.helpers import *
from mypath import Path
from util.select_gpu import lowest
from util.select_gpu import highest
import os
from mypath import Path
import re

def environ_or_default(key, default):
  try:
    x = os.environ[key]
    if type(default) == str:
      return str(x)
    if type(default) == int:
      return int(x)
    if type(default) == float:
      return float(x)
    return x
  except:
    return default

def object_to_string(o):
    o = str(o)
    # replace <a.b.c.foo object at 0x0000000> with <foo>
    o = re.sub("(<(\\w*\\.)*(\\w*)[^>]*>)", "<\\3>", o)
    return re.sub("\\s+", " ", o)  # trim (vertical) whitespace

def main():
    global loss_tr  # make all loss values available past main

    # Setting of parameters
    #seq_name = environ_or_default("SEQ_NAME", "blackswan")
    seq_name = "_jacket"
    res_prefix = environ_or_default("RES_PREFIX", "")

    db_root_dir = Path.db_root_dir()
    save_dir = Path.save_root_dir()
    snapshot_dir = Path.snapshot_dir()

    if not os.path.exists(save_dir):
        os.makedirs(os.path.join(save_dir))
    if not os.path.exists(snapshot_dir):
        os.makedirs(os.path.join(snapshot_dir))

    save_dir_res = os.path.join(save_dir, res_prefix+'Results', seq_name)
    if not os.path.exists(save_dir_res):
        os.makedirs(save_dir_res)
    if len(res_prefix) > 0:
        print("saving results in", save_dir_res)

    vis_net = 0  # Visualize the network?
    vis_res = 0  # Visualize the results?
    nAveGrad = 5  # Average the gradient every nAveGrad iterations, default: 5
    nEpochs = 2000  # nEpochs = 2000 * nAveGrad  # Number of epochs for training
    snapshot = nEpochs  # Store a model every snapshot epochs
    parentEpoch = 240  # default: 240
    parentModelName = "parent" # 'parentPytorch64iter'
    save_sideout = True

    # Parameters in p are used for the name of the model
    p = {
        'trainBatch': 1,  # Number of Images in each mini-batch
        }
    seed = 0

    progress_prints = 20  # number of times training progress is printed
    # runtime parameter validation
    if nEpochs % nAveGrad != 0:
        raise ValueError(str(nEpochs)+" must be multiple of "+str(nAveGrad))

    # Select which GPU, -1 if CPU
    # gpu_id = lowest("temperature.gpu", 0)
    gpu_id = highest("memory.free", 0)
    device = torch.device("cuda:"+str(gpu_id) if torch.cuda.is_available() and gpu_id >= 0 else "cpu")
    print(device)

    # Network definition
    net = vo.OSVOS(pretrained=0)
    net.load_state_dict(torch.load(os.path.join(snapshot_dir, parentModelName+'_epoch-'+str(parentEpoch-1)+'.pth'),
                                   map_location=lambda storage, loc: storage))

    # Logging into Tensorboard
    log_dir = os.path.join(save_dir, 'runs', datetime.now().strftime('%b%d_%H-%M-%S') + '_' + socket.gethostname()+'-'+seq_name)
    writer = SummaryWriter(log_dir=log_dir)

    net.to(device)  # PyTorch 0.4.0 style

    # Visualize the network
    if vis_net:
        x = torch.randn(1, 3, 480, 854)
        x.requires_grad_()
        x = x.to(device)
        y = net.forward(x)
        g = viz.make_dot(y, net.state_dict())
        g.view()

    # Use the following optimizer
    lr = 1e-8  # for SGD
    #lr = 1e-5  # for Adam
    wd = 0.0002
    optimizer = optim.SGD([
    #optimizer = optim.Adam([
        {'params': [pr[1] for pr in net.stages.named_parameters() if 'weight' in pr[0]], 'weight_decay': wd},
        {'params': [pr[1] for pr in net.stages.named_parameters() if 'bias' in pr[0]], 'lr': lr * 2},
        {'params': [pr[1] for pr in net.side_prep.named_parameters() if 'weight' in pr[0]], 'weight_decay': wd},
        {'params': [pr[1] for pr in net.side_prep.named_parameters() if 'bias' in pr[0]], 'lr': lr*2},
        {'params': [pr[1] for pr in net.upscale.named_parameters() if 'weight' in pr[0]], 'lr': 0},
        {'params': [pr[1] for pr in net.upscale_.named_parameters() if 'weight' in pr[0]], 'lr': 0},
        {'params': net.fuse.weight, 'lr': lr/100, 'weight_decay': wd},
        {'params': net.fuse.bias, 'lr': 2*lr/100},
        #], lr=lr)
        ], lr=lr, momentum=0.9)

    # Preparation of the data loaders
    # Define augmentation transformations as a composition
    composed_transforms = transforms.Compose([tr.RandomHorizontalFlip(),
                                              tr.ScaleNRotate(rots=(-30, 30), scales=(.75, 1.25)),
                                              tr.ToTensor()])
    #composed_transforms = transforms.Compose([tr.ToTensor()])

    print("parameters: nEpochs=%d, model=%s, nAveGrad=%d, batch_size=%d, transforms=%s, lr=%s" % (nEpochs, parentModelName+"_"+str(parentEpoch), nAveGrad, p['trainBatch'], object_to_string(composed_transforms), lr))

    # Training dataset and its iterator
    #use_indices = [0]
    #use_indices = list(range(15))
    #use_indices = list(range(15)) + [64]
    #use_indices = list(range(15)) + [44,64]
    #use_indices = [0,64]
    #use_indices = list(range(23)) + [44,64]
    #use_indices = list(range(33)) + [44,64]
    use_indices = [0,1,2]

    db_train = db.DAVIS2016(train=True, db_root_dir=db_root_dir, transform=composed_transforms, seq_name=seq_name, use_indices=use_indices)
    trainloader = DataLoader(db_train, batch_size=p['trainBatch'], shuffle=True, num_workers=1)

    # Testing dataset and its iterator
    db_test = db.DAVIS2016(train=False, db_root_dir=db_root_dir, transform=tr.ToTensor(), seq_name=seq_name)
    testloader = DataLoader(db_test, batch_size=1, shuffle=False, num_workers=1)

    num_img_tr = len(trainloader)
    num_img_ts = len(testloader)
    loss_tr = []
    aveGrad = 0

    print("Start of Online Training, sequence: " + seq_name)
    start_time = timeit.default_timer()
    # Main Training and Testing Loop
    for epoch in range(0, nEpochs):
        # One training epoch
        running_loss_tr = 0
        np.random.seed(seed + epoch)

        for ii, sample_batched in enumerate(trainloader):

            inputs, gts = sample_batched['image'], sample_batched['gt']

            # Forward-Backward of the mini-batch
            inputs.requires_grad_()
            inputs, gts = inputs.to(device), gts.to(device)

            outputs = net.forward(inputs)

            # Compute the fuse loss
            loss = class_balanced_cross_entropy_loss(outputs[-1], gts, size_average=False)
            #loss = imbalanced_cross_entropy_loss(outputs[-1], gts, size_average=False)
            running_loss_tr += loss.item()  # PyTorch 0.4.0 style

            # Print stuff
            if nEpochs < progress_prints or epoch % (nEpochs//progress_prints) == (nEpochs//progress_prints - 1):
                running_loss_tr /= num_img_tr
                loss_tr.append(running_loss_tr)

                print('[Epoch: %d, numImages: %5d]' % (epoch+1, ii + 1))
                print('Running Loss: %f' % running_loss_tr)
                writer.add_scalar('data/total_loss_epoch', running_loss_tr, epoch)

            # Backward the averaged gradient
            loss /= nAveGrad
            loss.backward()
            aveGrad += 1

            # Update the weights once in nAveGrad forward passes
            if aveGrad % nAveGrad == 0:
                writer.add_scalar('data/total_loss_iter', loss.item(), ii + num_img_tr * epoch)
                optimizer.step()
                optimizer.zero_grad()
                aveGrad = 0

        # Save the model
        if (epoch % snapshot) == snapshot - 1 and epoch != 0:
            torch.save(net.state_dict(), os.path.join(snapshot_dir, res_prefix + seq_name + '_epoch-'+str(epoch) + '.pth'))

    stop_time = timeit.default_timer()
    print('Online training time: ' + str(stop_time - start_time))

    # Testing Phase
    if vis_res:
        try:
            import matplotlib
            matplotlib.use("tkagg")  # ubuntu
            import matplotlib.pyplot as plt
            plt.close("all")
            plt.ion()
            f, ax_arr = plt.subplots(1, 3)
        except Exception as e:  # if there is a problem with the visualization, keep going with the testing
            logging.exception(e)
            print("Continue without visualization")
            vis_res = 0

    print('Testing Network')
    with torch.no_grad():  # PyTorch 0.4.0 style
        # Main Testing Loop
        for ii, sample_batched in enumerate(testloader):

            img, gt, fname = sample_batched['image'], sample_batched['gt'], sample_batched['fname']

            # Forward of the mini-batch
            inputs, gts = img.to(device), gt.to(device)

            outputs = net.forward(inputs)

            for jj in range(int(inputs.size()[0])):
                pred = np.transpose(outputs[-1].cpu().data.numpy()[jj, :, :, :], (1, 2, 0))
                pred = 1 / (1 + np.exp(-pred))
                pred = np.squeeze(pred)

                # Save the result, attention to the index jj
                sm.imsave(os.path.join(save_dir_res, os.path.basename(fname[jj]) + '.png'), pred)

                # save side output images
                if save_sideout:
                    for i, im in enumerate(outputs[:-1]):
                        im = np.transpose(im.cpu().data.numpy()[jj, :, :, :], (1, 2, 0))
                        im = 1 / (1 + np.exp(-im))
                        im = np.squeeze(im)
                        sm.imsave(os.path.join(save_dir_res, os.path.basename(fname[jj]) + "_"+str(i)+ '.png'), im)

                if vis_res:
                    img_ = np.transpose(img.numpy()[jj, :, :, :], (1, 2, 0))
                    gt_ = np.transpose(gt.numpy()[jj, :, :, :], (1, 2, 0))
                    gt_ = np.squeeze(gt_)  # remove dimensions with only one element, here: color channel
                    # Plot the particular example
                    ax_arr[0].cla()
                    ax_arr[1].cla()
                    ax_arr[2].cla()
                    ax_arr[0].set_title('Input Image')
                    ax_arr[1].set_title('Ground Truth')
                    ax_arr[2].set_title('Detection')
                    ax_arr[0].imshow(im_normalize(img_))
                    ax_arr[1].imshow(gt_)
                    ax_arr[2].imshow(im_normalize(pred))
                    plt.pause(0.001)

    writer.close()


if __name__ == "__main__":
    if os.name == "nt":  # windows
        torch.multiprocessing.freeze_support()
    main()
