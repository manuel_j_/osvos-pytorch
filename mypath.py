from util.path_abstract import PathAbstract
import base64
import os
import os.path


class Path(PathAbstract):
    @staticmethod
    def db_root_dir():
        # return '/path/to/DAVIS-2016'
        return os.path.expanduser("~/Desktop/davis2016")

    @staticmethod
    def save_root_dir():
        return './models'

    @staticmethod
    def models_dir():
        return "./models"

    @staticmethod
    def snapshot_dir():
        return "/data/vsa_manuel/osvos-models"


def decode(s):
    return base64.b64decode(s.encode()).decode()

