import os
from mypath import Path
from train_online import main
import torch
import time

f = open(Path.db_root_dir()+"/ImageSets/480p/val.txt")
txt = f.read()
f.close()
sets = set([s.split("/")[3] for s in txt.split()])
sets = sorted(sets)
print(sets)

def environ_or_default(key, default):
  try:
    x = os.environ[key]
    if type(default) == str:
      return str(x)
    if type(default) == int:
      return int(x)
    if type(default) == float:
      return float(x)
    return x
  except:
    return default

start = environ_or_default("START", 0)
end = environ_or_default("END", len(sets))
#length = end-start
times = []  # contains last 2 durations

def list_rolling_avg(li):
  if len(li) == 1:
    return li[0]
  return (li[-2]+li[-1]*2)/3

for i,s in enumerate(sets):
  if i < start:
    continue
  if i >= end:
    break
  os.environ["SEQ_NAME"] = s
  print(i+1, "/", len(sets))
  startTime = time.time()
  main()  # main computation
  duration = time.time()-startTime
  if len(times) > 1:
    times.pop(0)
  times.append(duration)
  predictedDuration = list_rolling_avg(times)
  totalLeft = predictedDuration*(end-i-1)
  t = totalLeft/60
  tstr = "%d minutes" % t if t < 60 else "%f hours" % (t/60)
  print("remaining time", tstr)
  try:
    torch.cuda.empty_cache()  # in case gpu id changes
  except RuntimeError as err:
    print(err)
    pass
