import os
import re


def environ_or_default(key, default):
    try:
        x = os.environ[key]
        if type(default) == str:
            return str(x)
        if type(default) == int:
            return int(x)
        if type(default) == float:
            return float(x)
        return x
    except:
        return default


def object_to_string(o):
    o = str(o)
    # replace <a.b.c.foo object at 0x0000000> with <foo>
    o = re.sub("(<(\\w*\\.)*(\\w*)[^>]*>)", "<\\3>", o)
    return re.sub("\\s+", " ", o)  # trim (vertical) whitespace
