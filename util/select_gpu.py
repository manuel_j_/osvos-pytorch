import os
from typing import List, Tuple


def cmd(query_gpu: str, prefix="\"") -> str:
    return prefix+"nvidia-smi\" --query-gpu=" + query_gpu + " --format=csv,noheader,nounits"


def query(query_gpu: str) -> List[int]:
    win_prefix = "\"C:\\Program Files\\NVIDIA Corporation\\NVSMI\\"
    out = os.popen(cmd(query_gpu)).read()
    if out == "":
        out = os.popen(cmd(query_gpu, prefix=win_prefix)).read()
    if out == "":
        raise FileNotFoundError("nvidia-smi not found")

    try:
        nums = [int(l) for l in out.split()]
        return nums
    except ValueError as e:
        raise TypeError("nvidia-smi gave non numeric output", out) from e


def sorted_with_indices(query_gpu: str) -> List[Tuple[int, int]]:
    # zip_ = zip(out, list(range(out.__len__())))
    # return sorted(zip_, key=lambda x: x[0])
    with_indices: List[Tuple[int, int]] = enumerate(query(query_gpu))
    return sorted(with_indices, key=lambda x: x[1])


def lowest(query_gpu: str, error_default: int=-1) -> int:
    """
    Get the GPU id with the lowest given stat

    :param error_default: if set, returns given value when an exception occurs
    :param query_gpu: run nvidia-smi --help-query-gpu
    :return: GPU id
    """
    try:
        return sorted_with_indices(query_gpu)[0][0]
    except (FileNotFoundError, TypeError) as e:
        if error_default >= 0:
            print(__file__, "warning: returned default value due to error", e)
            return error_default
        raise


def highest(query_gpu: str, error_default: int=-1) -> int:
    """
    Get the GPU id with the highest given stat

    :param error_default: if set, returns given value when an exception occurs
    :param query_gpu: run nvidia-smi --help-query-gpu
    :return: GPU id
    """
    try:
        return sorted_with_indices(query_gpu)[-1][0]
    except (FileNotFoundError, TypeError) as e:
        if error_default >= 0:
            print(__file__, "warning: returned default value due to error", e)
            return error_default
        raise


if __name__ == "__main__":
    print("gpu id with the lowest temperature")
    print(lowest("temperature.gpu"))
    try:
        print("exception for invalid query")
        print(highest("foo.bar"))
    except TypeError as e1:
        import logging
        logging.exception(e1)
