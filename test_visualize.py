# Package Includes
from __future__ import division

import os

# PyTorch includes
import torch
from torch.utils.data import DataLoader

# Custom includes
from dataloaders import davis_2016 as db
from dataloaders import custom_transforms as tr
from dataloaders.helpers import *
from mypath import Path


# Setting of parameters
if 'SEQ_NAME' not in os.environ.keys():
    seq_name = 'blackswan'
else:
    seq_name = str(os.environ['SEQ_NAME'])

db_root_dir = Path.db_root_dir()

# Testing dataset and its iterator
db_test = db.DAVIS2016(train=False, db_root_dir=db_root_dir, transform=tr.ToTensor(), seq_name=seq_name)
testloader = DataLoader(db_test, batch_size=1, shuffle=False, num_workers=1)


def main():
    # Testing Phase
    import matplotlib
    # matplotlib.use("tkagg")  # ubuntu
    import matplotlib.pyplot as plt
    plt.close("all")
    plt.ion()
    f, ax_arr = plt.subplots(1, 2)

    with torch.no_grad():  # PyTorch 0.4.0 style
        # Main Testing Loop
        for ii, sample_batched in enumerate(testloader):

            img, gt, fname = sample_batched['image'], sample_batched['gt'], sample_batched['fname']
            """
            if ii == 0: continue
            print(type(gt))
            print(gt.shape)
            print(gtx[0][0].shape)
            print(gtx.sum())
            gty: np.ndarray = gt.numpy()
            print(type(gty))
            print(gty.shape)
            print(gty.sum())
            gtx: torch.Tensor = gt
            if 0 is 0: return
            print(fname)
            """
            gtx: torch.Tensor = gt
            if gtx.sum() == 0:
                raise AssertionError("missing a ground truth image at iteration "+str(ii))

            for jj in range(int(img.size()[0])):
                img_ = np.transpose(img.numpy()[jj, :, :, :], (1, 2, 0))
                gt_ = np.transpose(gt.numpy()[jj, :, :, :], (1, 2, 0))
                gt_ = np.squeeze(gt_)
                # Plot the particular example
                ax_arr[0].cla()
                ax_arr[1].cla()
                ax_arr[0].set_title('Input Image')
                ax_arr[1].set_title('Ground Truth')
                ax_arr[0].imshow(im_normalize(img_))
                ax_arr[1].imshow(gt_)
                plt.pause(0.001)


if __name__ == "__main__":
    if os.name == "nt":  # windows
        torch.multiprocessing.freeze_support()
    main()
